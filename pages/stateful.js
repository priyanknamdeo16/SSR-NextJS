import React from "react";
import Head from "next/head";
import Layout from "../components/layout";
export default class stateful extends React.Component {
  static async getInitialProps({
    req,
    pathname,
    query,
    asPath,
    res,
    jsonPageRes,
    err
  }) {
    const userAgent = req ? req.headers["user-agent"] : navigator.userAgent;
    return { userAgent, name: "priyank" };
  }

  render() {
    return (
      <React.Fragment>
        <Layout>
          <Head>
            <meta
              name="viewport"
              content="initial-scale=1.2, width=device-width"
            />
          </Head>{" "}
          <div> World {this.props.userAgent}</div>
          <div>{this.props.name}</div>
        </Layout>
      </React.Fragment>
    );
  }
}
