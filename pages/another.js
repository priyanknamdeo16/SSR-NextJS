import Router from "next/router";

export default () => (
  <div>
    typically you want to use `next/link` for this usecase but this example
    shows how you can also access the router and use it manually.
    <p>
      {" "}
      Click <span onClick={() => Router.push("/about")}>here</span> to read more
      - to go to about on client side
    </p>
  </div>
);
