import Layout from "../components/layout";
import Head from "next/head";

//title can get overritten
var Index = () => (
  <Layout>
    <Head>
      <title>About</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <h1>About Update</h1>
  </Layout>
);
export default Index;
