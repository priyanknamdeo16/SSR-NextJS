import Layout from "../components/layout";
import Fetch from "isomorphic-unfetch";
import Link from "next/link";
import Head from "next/head";

var Index = props => (
  <Layout>
    <Head>
      <meta name="viewport" content="initial-scale=1.2, width=device-width" />
    </Head>
    <h1>Welocme123A </h1>
    <div style={{ color: "red" }}> Time: {new Date().getTime()}</div>
    <div> Task Id: {props.id}</div>
    <div> Task Title: {props.title}</div>
    <Link href="/about" replace>
      <img src="/static/ph.jpg" alt="my image" />
    </Link>
    <Head>
      <title>Home</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.3, width=device-width" />
    </Head>
  </Layout>
);

Index.getInitialProps = async function() {
  const res = await fetch("https://jsonplaceholder.typicode.com/todos/1");
  const data = await res.json();
  return {
    id: data.id,
    title: data.title
  };
};

export default Index;
