import Link from "next/link";

const NavBar = () => (
  <nav className="navbar navbar-expand navbar-dark bg-dark mb-4">
    <div className="container">
      <a className="navbar-brand" href="#">
        Price
      </a>
      <div className="collapse navbar-collapse">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link prefetch href="/">
              <a className="nav-link">Home</a>
            </Link>
          </li>
          <li className="nav-item">
            <Link href="/about" replace>
              <div>
                <a className="nav-link">About (Replace)</a>
              </div>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              scroll={false}
              href={{ pathname: "/stateful", query: { name: "Zeit" } }}
            >
              <a className="nav-link">Stateful (Push)</a>
            </Link>
            <li className="nav-item">
              <Link href="/another">
                <a className="nav-link">ANOTHER</a>
              </Link>
            </li>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  //   <div>
  //     <ul>
  //       <li>
  //         <Link href="/">
  //           <a>Go to Home</a>
  //         </Link>
  //       </li>
  //       <li>
  //         <Link href="/about">
  //           <a>Go to About</a>
  //         </Link>
  //       </li>
  //     </ul>
  //     <style jsx>{`
  //       ul {
  //         color: red;
  //       }
  //       ul li {
  //         font-size: 18px;
  //       }
  //       ul li a {
  //         color: blue;
  //       }
  //     `}</style>
  //   </div>
);

export default NavBar;
