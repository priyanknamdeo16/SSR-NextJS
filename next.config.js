module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders }) => {
    // Perform customizations to webpack config

    console.log(config.module.rules);
    // defaultLoaders.push({
    //   test: /\.(jpe?g|png|gif|svg)$/i,
    //   loader: "file-loader?name=/public/icons/[name].[ext]"
    // });

    // Important: return the modified config
    return config;
  },
  webpackDevMiddleware: config => {
    // Perform customizations to webpack dev middleware config

    // Important: return the modified config
    return config;
  }
};
